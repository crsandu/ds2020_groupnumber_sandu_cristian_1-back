import fs from 'fs'

// get server configuration
const configData = JSON.parse(fs.readFileSync('./src/configuration/config.json'))['configuration']

export default {
    serverPort: Number(configData['port']),
    securityFilters: configData['security-filters'] == 'true',
    originsList: configData['allow-origin'].split(' '),
    connStrDB: configData['connection-string'],
    dbSyncForce: configData['dbForce-Sync'] == 'true',
    jwtHashToken: configData['jwt-hashToken'],
    listenPort: configData['listenPort']
}