import express from 'express'

import iConfig from './configuration/iConfig'
import iRouter from './routes/iRouter'
import iController from './controllers/iController'
import iMediator from './mediator/iMediator'

// Wrapper to force the AWAIT
(async () => {
        const routerRoot = express();

        const control = await iController;
        const mediator = iMediator(control);
        const router = await iRouter(iConfig, mediator);

        routerRoot.use(router);

        const server = routerRoot.listen(iConfig.serverPort, '0.0.0.0', () => {
            console.log(`Application start listening at port:${iConfig.serverPort}`)
        });

        process.on('SIGINT', async () => {
            console.log( "\nGracefully shutting down from SIGINT (Ctrl-C)" );
            await server.close(() => {
                console.log('Http server closed.');
            });
            process.exit(0); 
        });

        process.on('uncaughtException', async () => {
            console.log( "\nSystem crashed!" );
            await server.close(() => {
                console.log('Http server closed.');
            });
            process.exit(1); 
        })
})();

