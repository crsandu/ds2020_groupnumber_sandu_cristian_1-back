Mediator will specify which paths corresponds to which controller. Also it specify the permissions
needed to acces that controller function.

Routers ----> Mediator ----> Controller

Export format
- path:         - route path
- method:       - post / get - query type
- group:        - permission group {anonymous, pacient, caregiver, doctor} (*1)
- action:       - which action to execute on matching
- parameters:   - which parameters are allowed. It is required if specified, 
                    else it is optional.

*1 - for anonymous there is no token required.

This is only an interface for Controller

More info about REST API: https://www.restapitutorial.com/lessons/httpmethods.html
