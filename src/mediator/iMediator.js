/*  Export format
**  - path:string       - route path
**  - method:string     - post / get - query type
**  - group:list        - permission group {anonymous, pacient, caregiver, doctor}
**  - action:function   - which action to execute on matching
**  - parameters:list (optional)   - list of parameters which are requiered for this method
**  @returns: an array of mediators
*/
export default async (controller) => (
    [
        // listener functions (ya, ya, I know it is careggiver, but I have not time to correct it)
        { path: '/getNotif', method: 'post', group: ['Anonymous'], 
            action: controller.listener.GetNotification },

        // login functions
        { path: '/auth/register', method: 'post', group: ['Anonymous'], 
            action: controller.auth.Register, parameters: ['username', 'password', 'email'] },
        { path: '/auth/login',  method: 'post', group: ['Anonymous'], 
            action: controller.auth.Login, parameters: ['username', 'password'] },
        
        // general user actions
        { path: '/actions/us/viewProfile', method: 'post', group: ['Pacient', 'Caregiver', 'Doctor'], 
            action: controller.user.ViewProfile },  // no parameters needed, all data is stored in token

        // pacients actions
        { path: '/actions/pt/viewMedication', method: 'post', group: ['Pacient'], 
            action: controller.pacient.GetMedication }, // no parameters needed, all data is stored in token

        // caregiver actions
        { path: '/actions/cg/viewPacients', method: 'post', group: ['Caregiver'], 
            action: controller.caregiver.ViewPacients },
        { path: '/actions/cg/viewUser', method: 'post', group: ['Caregiver'], 
            action: controller.caregiver.ViewPacient },
        { path: '/actions/cg/viewMedication', method: 'post', group: ['Caregiver'], 
            action: controller.caregiver.ViewMedication, parameters: ['id'] },
        { path: '/actions/cg/viewMdOfPct', method: 'get', group: ['Caregiver'], 
            action: controller.caregiver.GetMdOfPct, parameters: ['pacientID'] },

        // doctor actions
        { path: '/actions/dc/createMed', method: 'post', group: ['Doctor'],
            action: controller.doctor.CreateMedication, parameters: ['pacientID', 'title', 'drugs'] },
        
        { path: '/actions/dc/viewMed', method: 'get', group: ['Doctor'],
            action: controller.doctor.ViewMedication, parameters: ['medID'] },
        { path: '/actions/dc/viewMeds', method: 'post', group: ['Doctor'], 
            action: controller.doctor.ViewMedications },

        { path: '/actions/dc/viewMedUser', method: 'get', group: ['Doctor'],
            action: controller.doctor.ViewMedicationPacient, parameters: ['userID'] },

        { path: '/actions/dc/updateMed', method: 'put', group: ['Doctor'], 
            action: controller.doctor.UpdateMedication, parameters: ['medID', 'drugs'] } ,
        { path: '/actions/dc/deleteMed', method: 'delete', group: ['Doctor'], 
            action: controller.doctor.DeleteMedication, parameters: ['medID'] },

        { path: '/actions/dc/createUsr', method: 'post', group: ['Doctor'], 
            action: controller.doctor.CreateUser, parameters: ['username', 'password', 'email', 'role'] },
        { path: '/actions/dc/viewUsr', method: 'post', group: ['Doctor'], 
            action: controller.doctor.ViewUser, parameters: ['userID'] },
        { path: '/actions/dc/viewUsrs', method: 'post', group: ['Doctor'], 
            action: controller.doctor.ViewUsers },
        { path: '/actions/dc/modifyUsr', method: 'put', group: ['Doctor'], 
            action: controller.doctor.ModifyUser, parameters: ['userID', 'username', 'password', 'email', 'role']  },
        { path: '/actions/dc/deleteUsr', method: 'delete', group: ['Doctor'], 
            action: controller.doctor.DeleteUser, parameters: ['userID'] } ,

        { path: '/actions/dc/chgRole', method: 'put', group: ['Anonymous'], 
            action: controller.doctor.ChangeUserRole, parameters: ['userID', 'role'] },
        { path: '/actions/dc/addPacientToCg', method: 'post', group: ['Doctor'], 
            action: controller.doctor.AddPacientToCg, parameters: ['pacientID', 'caregiverID'] },
        { path: '/actions/dc/deletePacientFromCg', method: 'delete', group: ['Doctor'], 
            action: controller.doctor.RemovePacientFromCg, parameters: ['cgID'] } ,
        { path: '/actions/dc/viewCgs', method: 'post', group: ['Doctor'], 
            action: controller.doctor.ViewCaregivers },
        { path: '/actions/dc/addCgs', method: 'post', group: ['Doctor'], 
            action: controller.doctor.AddCaregiver }
    ]
);