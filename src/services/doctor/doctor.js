export default {
    AddPacientToCg: async (repo, params) => {
        // can not have the same ID
        if(params['pacientID'] === params['caregiverID']) {
            return {
                status: 401,
                message: {
                    args: 'can not add the pacient to db'
                }
            }
        }

        // check if pacientID & caregiverID exists in DB
        const pacient = await repo.user.GetUser({
            where: {
                id: params['pacientID']
            },
            reqParams: ['role']
        });
        const caregiver = await repo.user.GetUser({
            where: {
                id: params['caregiverID']
            },
            reqParams: ['role']
        });
        console.log(caregiver)
        if(pacient.status !== 200 || caregiver.status !== 200) {
            return {
                status: 401,
                message: {
                    args: 'users not exists'
                }
            }
        }
        if(caregiver.message.role != 'Caregiver') {
            return {
                status: 401,
                message: {
                    args: 'there is no caregiver role'
                }
            }
        }
        return repo.pacient.AddPacient(params);
    }
};