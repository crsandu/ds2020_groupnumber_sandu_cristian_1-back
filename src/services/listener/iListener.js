import { NOMEM } from 'dns';
import net from 'net'
import config from '../../configuration/iConfig'

// temporal storage, cache
const messages = [];

export default {
  CreateListener: (params) => {
    // server for our socket (python script)
    net.createServer(function (socket) {
      socket.on('data', function (data) {
        messages.push(data.toString());
        console.log(messages);
      });
    }).listen(config.listenPort);
  },
  GetNotification: (params) => {
    if(messages.length === 0) {
      return {
        status: 404,
        message: 'No notification found!'
      }
    } else {
      const notMsg = messages.pop();
      console.log(notMsg);

      return {
        status: 200,
        message: notMsg
      };
    }
  }
};