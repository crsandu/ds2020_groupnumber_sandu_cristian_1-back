import Login from './login'

export default {
    Register: (repo, params) => repo.user.AddUser(params),
    Login : (repo, params) => Login(repo, params)
};