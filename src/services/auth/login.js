import jsonwebtoken from 'jsonwebtoken'
import config from '../../configuration/iConfig'

export default async (repo, params) => {
    const result = await repo.user.GetUser({
        where: params, 
        reqParams: ['id', 'role']
    });
    // login was succcesful
    if(result.status == 200) {
        const resBody = result.message;
        const accesToken = jsonwebtoken.sign({ 
            userID: resBody.id,
            role: resBody.role
        }, config.jwtHashToken);
        // add token in the message
        result.message = {
            token: accesToken
        };
    }
    return result;
};