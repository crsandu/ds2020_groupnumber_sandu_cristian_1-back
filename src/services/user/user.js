export default {
    GetProfile: (repo, params) => {
        return repo.user.GetUser({
            where: params,
            reqParams: ['username', 'role']
        });
    },
    UpdateUserRole: (repo, params) => {
        return repo.user.UpdateUserRole({
            where: params['identify'],
            updateValue: params['replace']
        });
    },
    UpdateUser: (repo, params) =>  repo.user.UpdateUser(params),
    GetUserEx: (repo, params) => {
        console.log(repo.user);
        return repo.user.GetUser({
            where: params,
            reqParams: ['id', 'username', 'password', 'email', 'role']
        });
    },
    GetUsers: (repo, params) => {
        return repo.user.GetUsers({
            reqParams: ['id', 'username', 'role']
        });
    },
    DeleteUser: (repo, params) => {
        const deleteUser = repo.user.DeleteUser(({
            where: params
        }));
        // could not delete the user
        if(deleteUser.status !== 200) {
            return deleteUser;
        }
    }
};