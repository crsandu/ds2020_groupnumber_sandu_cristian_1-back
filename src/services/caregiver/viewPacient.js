/*  This method will return pacient information, only if the pacient
    belongs to the caregiver
*/
export default (repo, params) => {
    return repo.pacient.view(params);
};