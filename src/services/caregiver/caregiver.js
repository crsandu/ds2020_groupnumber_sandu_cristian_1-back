import GetPacients from './getPacients'
import ViewPacient from './viewPacient'

export default {
    GetPacients: (repo, params) => GetPacients(repo, params),
    ViewPacient: (repo, params) => ViewPacient(repo, params),
    GetCaregivers: (repo, params) => repo.caregiver.GetCaregivers(params),
    DeleteCaregiver: (repo, params) => repo.caregiver.DeleteCaregiver(params),
};