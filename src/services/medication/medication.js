export default {
    AddMedication: (repo, params) => repo.medication.AddMed(
        params, 
        { include: [repo.models.user] }
    ),

    GetMedication: (repo, params) => repo.medication.GetMed({ 
        where: params, 
        include: [repo.models.user], reqParams: ['medTitle', 'drugs'] 
    }),
    GetMedications: (repo, params) => repo.medication.GetMeds({ 
        where: params['where'], 
        reqParams: params['reqParams'],
        include: [repo.models.user]
    }),

    UpdateMedication: (repo, params) => repo.medication.UpdateMeds({
        where: params['indentify'],
        updateValue: params['replace']
    }),
    DeleteMedication: (repo, params) => repo.medication.DeleteMeds(params)
};