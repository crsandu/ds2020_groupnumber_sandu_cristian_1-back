import auth from './auth/auth'
import user from './user/user'
import caregiver from './caregiver/caregiver'
import doctor from './doctor/doctor';
import medication from './medication/medication'
import listener from './listener/iListener'

/*  This file is an interface for a service package, it will return a dictionary
**  containing all methods which can be accessed by service class
*/
export default (repo) => ({
    auth: {
        Register: (params) => auth.Register(repo, params),
        Login: (params) => auth.Login(repo, params)
    },

    medication: {
        AddMedication: (params) => medication.AddMedication(repo, params),
    
        GetMedication: (params) => medication.GetMedication(repo, params),
        GetMedications: (params) => medication.GetMedications(repo, params),

        UpdateMedication: (params) => medication.UpdateMedication(repo, params),
        DeleteMedications: (params) => medication.DeleteMedication(repo, params)
    },

    user: {
        GetProfile: (params) => user.GetProfile(repo, params),
        UpdateUser: (params) => user.UpdateUser(repo, params),
        
        GetUsers: (params) => user.GetUsers(repo, params),
        GetUserEx: (params) => user.GetUserEx(repo, params),

        UpdateUserRole: (params) => user.UpdateUserRole(repo, params),
        DeleteUser: (params) => user.DeleteUser(repo, params)
    },

    caregiver: {
        ViewPacient: (params) => caregiver.ViewPacient(repo, params),
        ViewPacients: (params) => caregiver.ViewPacients(repo, params),
        GetCaregivers: (params) => caregiver.GetCaregivers(repo, params),
        DeleteCaregiver: (params) => caregiver.DeleteCaregiver(repo, params)
    },

    doctor: {
        AddPacientToCg: (params) => doctor.AddPacientToCg(repo, params)
    },

    listener: {
        CreateListener: (params) => listener.CreateListener(params),
        GetNotification: (params) => listener.GetNotification(params)
    }
});