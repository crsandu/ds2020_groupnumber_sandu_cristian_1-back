function IsArraysEqual(strA, strB) {
    if (strA === strB) return true;
    if (strA == null || strB == null) return false;
    if (strA.length !== strB.length) return false;

    const sortedA = strA.sort();
    const sortedB = strB.sort();
    for (let i = 0; i < strA.length; ++i) {
        if (sortedA[i] !== sortedB[i]) {
            return false;
        }
    }
  return true;
}

/*  This filter will check if the parameters match with
    with the one which are specified in mediator
    The params from the called will be passed down in the
    list made by factoryRouters function.
*/
export default (reqParams) => {
    // This will be called by router in the factoryRouters, after
    // the token has been verified.
    return (req, res, next) => {
        const paramsKeys = Object.keys(req.body);
        
        // req. parameters match
        if(!IsArraysEqual(paramsKeys, reqParams)) {
            res.status(403).json({ error: 'Bad parameters!' });
        } else {
            next();
        }
    }
};