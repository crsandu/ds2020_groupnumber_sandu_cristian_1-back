/*  This is the permission filter for groups.
    This function must be called only after authFilter, because it contains
    the extracted token.
    If the request's user role is not in the permission group, next function will 
    not be called.
*/
export default (groups) => {
    // create a express function for this filter
    return (req, res, next) => {
        // token was saved from authFilter in req.ExtractedToken
        const token = req.ExtractedToken;
        const role = token.role;  // obtinem grupul utilizatorului

        if(groups.includes(role)) {
            next();
        } else {
            // error
            res.status(405).json({ error: `You are ${role} and have no permissions to access this!` });
        }
    }
};