import jsonwebtoken from 'jsonwebtoken'

import config from '../../../../configuration/iConfig'

/*  This function will verify the JWT authentification token. 
    Also, if the token is valid, express.next() will be called and
    token will be stored in req.ExtractedToken.
    If the authentification is not valid, next function will never be called
*/
export default (req, res, next) => {
    // Information is kept as Bearer Token
    const token = req.headers.authorization;

    // No header for authentification
    if(!token) {
        res.status(403).json({ error: 'No credentials sent!' });
        return;
    }

    // try decoding the received JWT
    const strToken = req.headers.authorization.split(' ')[1];
    try {
        const extractedToken = jsonwebtoken.verify(strToken, config.jwtHashToken, { algorithms: ['HS256'] });
        // save extracted token for next call
        req.ExtractedToken = extractedToken;
    } catch (err) {
        res.status(403).json({ error: 'JWT token malformed!' });
        return;
    }

    // everything worked okay -> go to next level
    next();
};