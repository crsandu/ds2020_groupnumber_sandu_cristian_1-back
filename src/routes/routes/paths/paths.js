import express from 'express'

import CrRespond from './factoryRouters'

/*  README.md 
*/
export default async (mediator) => {
    const mainRouter = express()
    const aMediator = await mediator;

    // parcurgem mediatorul pentru a genera layer-ul
    for(let i = 0; i < aMediator.length; i++) {
        const control = aMediator[i];
        const fCall = CrRespond(
            control.path,
            control.method,
            control.group,
            control.action,
            control.parameters
        );
        mainRouter.use(fCall);
    }
    return mainRouter;
};