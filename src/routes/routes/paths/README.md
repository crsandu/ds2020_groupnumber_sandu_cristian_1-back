This folder contains all paths which can be called by controller. There are some rules, when calling the paths
- unprotected - contains the paths where JWT is not mandatory
- protected - contains the paths with security levels. JWT authentification token is mandatory

This is a factory DP which will where paths.js will create a router of routers for for every page.