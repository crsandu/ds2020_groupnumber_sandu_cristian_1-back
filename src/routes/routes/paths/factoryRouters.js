import express from 'express'

import AuthFilter from './filters/authFilter'
import GetPermFilter from './filters/permFilter'
import GetParamFilter from './filters/paramsFilter'

function GetFunction(method) {
    const router = express();

    switch(method) {
        case 'post': return router.post.bind(router);
        case 'get': return router.get.bind(router);
        case 'put': return router.put.bind(router);
        case 'delete': return router.delete.bind(router);
    }
}

/*  This function will return a routers array which are having the parameters from the 
**  send dictionary.
*/
export default (path, method, group, callback, parameters) => {
    const fMeth = GetFunction(method);

    // all callbacks which will be called by route function
    const fCalls = [];

    // Check filters, anonymous group is not verified
    if(!group.includes('Anonymous')) {
        // password validation
        fCalls.push(AuthFilter);
        fCalls.push(GetPermFilter(group));
    }

    if(parameters != undefined) {
        // time to put parameter filter
        fCalls.push(GetParamFilter(parameters));
    }

    // add our function
    fCalls.push(async (req, res) => {
        const params = req.body;
        params['__token'] = req.ExtractedToken; // this is available only for authentification

        const result = await callback(params);
        res.statusCode = result.status;

        const respBody = JSON.stringify(result.message);
        console.log(result.message);
        res.send(respBody);
    });

    return fMeth(path, fCalls);
}