import express from 'express'
import cors from 'cors';

export default (config) => {
    const router = express();
    if(config.securityFilters) {
        
        /*  Cross-Origin Resource Sharing (CORS)
            more info: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
        */
        router.use(cors({
            origin: (origin, callback) => {
                // accept requests from react only
                const isAllowed = !origin || config.originsList.indexOf(origin) !== -1;
                if(isAllowed) {
                    callback(null, true);
                } else {
                    callback({
                        statusCode: 401,
                        error: 'Not allowed',
                    });
                }
            },
            optionsSuccessStatus: 200
        }));
    }
    return router;
}