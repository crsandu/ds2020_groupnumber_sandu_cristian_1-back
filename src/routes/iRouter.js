import express from 'express'
import bodyParser from 'body-parser'

import NoFoundFilter from './routes/filters/notFoundFiliter'
import GetSecurityFilter from './routes/filters/securityFilter'

import GetPaths from './routes/paths/paths'

export default async (config, mediator) => {
    const routerRoot = express();

    // add security filters
    routerRoot.use(GetSecurityFilter(config));

    // add body parser for post-request
    routerRoot.use(bodyParser.json());
    routerRoot.use(bodyParser.urlencoded({extended: true}));

    // routing to pages
    const paths = await GetPaths(mediator);

    // mediator paths
    routerRoot.use(paths);

    // routing to protected pages
    routerRoot.use(NoFoundFilter);

    return routerRoot;
}