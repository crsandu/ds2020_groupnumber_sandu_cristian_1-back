import UserModel from './models/userModel'
import PacientModel from './models/pacientModel'
import MedModel from './models/medModel'

import GetSQLConn from './connectDB'

/*  This file is an interface for a service package, it will return a dictionary
**   containing all methods which can be accessed by service class
*/

export default async (config) => {
    const sqlConn = GetSQLConn(config);     // this is a function, in order to generate the connection
    const allModels = {
        user: await UserModel(sqlConn),
        caregiver: await PacientModel(sqlConn),
        med: await MedModel(sqlConn),
    };
    
    // check if db needs sync
    if(config.dbSyncForce) {
        for (const [key, value] of Object.entries(allModels)) {
            await value.sync({ force: true })
        }
    }

    // create connections
    await allModels.user.hasMany(allModels.caregiver, { foreignKey: 'pacientID', sourceKey: 'id' }); // 1:N
    await allModels.caregiver.belongsTo(allModels.user, { foreignKey: 'pacientID', sourceKey: 'pacientID' }); // N:1

    await allModels.user.hasMany(allModels.med, { foreignKey: 'userID', sourceKey: 'id' }); // 1:N
    await allModels.med.belongsTo(allModels.user, { foreignKey: 'userID', sourceKey: 'userID' }); // N:1

    return allModels;
}