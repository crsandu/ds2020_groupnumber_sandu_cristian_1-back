/*  This is an 1:N association between user and given
    caregiver
*/
import { DataTypes } from 'sequelize';

/*  This presents medicamentation model
*/
export default (seqConn) => seqConn.define('Medicamention', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  // the user which needs some drugs
  userID: {
    type: DataTypes.INTEGER,
    validate: {
      notEmpty: true
    }
  },
  // title for the drug prescription
  medTitle: {
    type: DataTypes.STRING,
    validate: {
      notEmpty: true
    }
  },
  drugs: {
    type: DataTypes.STRING,
    validate: {
      notEmpty: true
    }
  }
},
{
  // force uniquess of 2 columns combation of pacient + caregiver
  indexes: [
    {
        unique: true,
        fields: ['userID', 'medTitle']
    }
  ]
});