import { DataTypes } from 'sequelize';

export default (seqConn) => seqConn.define('User', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  username: {
    type: DataTypes.STRING,
    unique: true,
    validate: {
      notEmpty: true,
      len: [5, 20]  
    }
  },
  password: {
    type: DataTypes.STRING,
    validate: {
      notEmpty: true,
      len: [8, 32]
    }
  },
  email: {
    type: DataTypes.STRING,
    unique: true,
    validate: {
      notEmpty: true,
      isEmail: true
    }
  },

  // optional fields
  realname: {
    type: DataTypes.STRING
  },

  // autocomplete values
  role: {
    type: DataTypes.STRING,
    defaultValue: 'Pacient'
  }
});