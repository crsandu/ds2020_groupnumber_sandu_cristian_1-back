/*  This is an 1:N association between user and given
    caregiver
*/
import { DataTypes } from 'sequelize';

export default (seqConn) => seqConn.define('Pacient', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  pacientID: {
    type: DataTypes.INTEGER,
    validate: {
      notEmpty: false,
    }
  },
  caregiverID: {
    type: DataTypes.INTEGER,
    validate: {
      notEmpty: false,
    }
  }
},
{
  // force uniquess of 2 columns combation of pacient + caregiver
  indexes: [
    {
        unique: true,
        fields: ['pacientID', 'caregiverID']
    }
  ]
});