import Sequelize from 'sequelize';

// more information about this script: https://sequelize.org/master/manual/getting-started.html
// start connection to DB
export default (config) => {
    const seqConn = new Sequelize(config.connStrDB);
    seqConn.authenticate();
    return seqConn;
};