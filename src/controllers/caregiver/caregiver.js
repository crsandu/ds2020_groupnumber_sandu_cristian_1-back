export default {
    ViewPacient: (service, params) => {
        return service.caregiver.ViewPacient({
            pacientID: params['pacientID'],
            caregiverID: params['__token']['userID']
        });
    },
    ViewPacients: (service, params) => service.caregiver.GetCaregivers({
        where: {
            caregiverID: params['__token']['userID']
        },
        reqParams: ['pacientID']
    }),
    ViewPacient: (service, params) => service.user.GetUserEx({
        id: params['userID']
    }),
    ViewMedication: (service, params) => service.medication.GetMedication({
        userID: params['id']
    })
}