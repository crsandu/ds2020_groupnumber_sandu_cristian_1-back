export default {
    GetMedication: (service, params) => service.medication.GetMedications({
        where: {
            userID: params['__token']['userID']
        },
        reqParams: ['id', 'medTitle', 'drugs']
    })
}