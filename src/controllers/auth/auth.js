export default ({
    Register: (service, params) => service.auth.Register({
        username: params['username'],
        password: params['password'],
        email: params['email']
    }),
    Login: (service, params) => service.auth.Login({
        username: params['username'],
        password: params['password']
    })
});