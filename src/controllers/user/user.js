export default {
    ViewProfile: (service, params) => service.user.GetProfile({
        id: params['__token']['userID']
    }),
}