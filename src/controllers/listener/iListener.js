export default {
    CreateListener: (serv, params) => serv.listener.CreateListener(params),
    GetNotification: (serv, params) => serv.listener.GetNotification(params)
};