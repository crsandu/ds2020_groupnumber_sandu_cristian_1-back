// database mode & repository, low binding
import iService from '../services/iService'
import iRepository from '../repository/iRepository'
import iModel from '../models/iModel'
import iConfig from '../configuration/iConfig'

// next level of controllers
import auth from './auth/auth'
import doctor from './doctor/doctor'
import user from './user/user'
import pacient from './pacients/pacients'
import caregiver from './caregiver/caregiver'
import listener from './listener/iListener'

/*  This function will create connection needed for the core of
    application to run.
*/
async function GenerateController() {
    const iModels = await iModel(iConfig);
    const iRepo = iRepository(iModels);     // create repository based on actual models
    
    // show services
    const iServ = iService(iRepo);

    // start listener
    const listenServer = listener.CreateListener(iServ, undefined)

    return {
        // functiile pentru gestionarea autentificarii
        auth: {
            Register: (params) => auth.Register(iServ, params),
            Login: (params) => auth.Login(iServ, params)
        },

        // general user actions
        user: {
            ViewProfile: (params) => user.ViewProfile(iServ, params)
        },

        // actions on pacients
        pacient: {
            GetMedication: (params) => pacient.GetMedication(iServ, params)
        },
        
        // actions on caregiver
        caregiver: {
            GetPacients: (params) => caregiver.GetPacients(iServ, params),
            ViewPacient: (params) => caregiver.ViewPacient(iServ, params),
            ViewPacients: (params) => caregiver.ViewPacients(iServ, params),
            ViewMedication: (params) => caregiver.ViewMedication(iServ, params),
            GetMdOfPct: (params) => caregiver.GetMdOfPacient(iServ, params)
        },

        // actions on doctors
        doctor: {
            CreateMedication: (params) => doctor.CreateMedication(iServ, params),
            ViewMedication: (params) => doctor.ViewMedication(iServ, params),
            ViewMedicationPacient: (params) => doctor.ViewMedicationPacient(iServ, params),
            ViewMedications: (params) => doctor.ViewMedications(iServ, params),
            UpdateMedication: (params) => doctor.UpdateMedication(iServ, params),
            DeleteMedication: (params) => doctor.DeleteMedication(iServ, params),

            CreateUser: (params) => doctor.CreateUser(iServ, params),
            ViewUser: (params) => doctor.ViewUser(iServ, params),
            ViewUsers: (params) => doctor.ViewUsers(iServ, params),
            ModifyUser: (params) => doctor.ModifyUser(iServ, params),
            DeleteUser: (params) => doctor.DeleteUser(iServ, params),

            ChangeUserRole: (params) => doctor.ChangeUserRole(iServ, params),
            AddPacientToCg: (params) => doctor.AddPacientToCg(iServ, params),
            RemovePacientFromCg: (params) => doctor.RemovePacientFromCg(iServ, params),
            ViewCaregivers: (params) => doctor.ViewCaregivers(iServ, params)
        },

        listener: {
            GetNotification: (params) => listener.GetNotification(iServ, params)
        }
    };
};

export default GenerateController();