export default {
    CreateMedication: (service, params) => service.medication.AddMedication({
        userID: params['pacientID'],
        medTitle: params['title'],
        drugs: params['drugs']
    }),
    ViewMedication: (service, params) => service.medication.GetMedication({
        pacientID: params['pacientID']
    }),
    ViewMedications: (service, params) => service.medication.GetMedications({
        reqParams: ['id', 'medTitle', 'drugs', 'userID'] 
    }),
    ViewMedicationPacient: (service, params) => service.medication.GetMedications({
        userID: params['userID']
    }),
    UpdateMedication: (service, params) => service.medication.UpdateMedication({
        identify: {
            medID: params['medID'],
        },
        replace: {
            drugs: params['drugs']
        }
    }),
    DeleteMedication: (service, params) => service.medication.DeleteMedications({
        where: {
            id: params['medID']
        }
    }),

    CreateUser: (service, params) => service.auth.Register({
        username: params['username'],
        password: params['password'],
        email: params['email'],
        role: params['role']
    }),
    ViewUser: (service, params) => service.user.GetUserEx({
        id: params['userID']
    }),
    ViewUsers: (service, params) => service.user.GetUsers(),
    ModifyUser: (service, params) => service.user.UpdateUser({
        where: {
            id: params['userID']
        },
        updateValue: {
            username: params['username'],
            password: params['password'],
            email: params['email'],
            role: params['role']
        }
    }),
    DeleteUser: (service, params) => service.user.DeleteUser({
        id: params['userID']
    }),

    ChangeUserRole: (service, params) => service.user.UpdateUserRole({
        identify: {
            id: params['userID']
        },
        replace: {
            role: params['role']
        }
    }),
    
    AddPacientToCg: (service, params) => service.doctor.AddPacientToCg({
        pacientID: params['pacientID'],
        caregiverID: params['caregiverID']
    }),
    RemovePacientFromCg: (service, params) => service.caregiver.DeleteCaregiver({
        where: {
            id: params['cgID']
        }
    }),
    ViewCaregivers: (service, params) => service.caregiver.GetCaregivers({
        reqParams: ['id', 'pacientID', 'caregiverID']
    })
}