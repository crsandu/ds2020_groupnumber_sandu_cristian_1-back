import GetCaregivers from './getCaregivers'
import DeleteCaregiver from './deleteCaregiver'

/*  This script will manipulate the user data
*/
export default {
    GetCaregivers: (model, params) => GetCaregivers(model, params),
    DeleteCaregiver: (model, params) => DeleteCaregiver(model, params)
} 