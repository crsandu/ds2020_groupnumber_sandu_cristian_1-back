import assert from 'assert'

/*  This function will fetch all users from the database
*/
export default (model, params) => {
    console.log(model);
    return model.caregiver.findAll(params)
    .then((value) => {
        assert(value != null, 'No caregiver found!');

        const fRestul = [];
        for(let i=0; i<value.length; i++) {
            const result = {} 
            params.reqParams.forEach(el => {result[el] = value[i][el]});
            fRestul.push(result);
        }
        console.log(fRestul);
        return {
            status: 200,
            message: JSON.stringify(fRestul)
        };
    }).catch(err => {
        return {
            status: 400,
            message: {
                args: err.message
            }
        };
    })
}
