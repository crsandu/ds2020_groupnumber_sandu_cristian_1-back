import userManipulation from './user/user'
import pacientManipulation from './pacient/pacient'
import medManpilation from './medication/medication'
import caregiverManipulation from './caregiver/caregiver'
import deleteCaregiver from '../../../frontend-platform/src/actions/caregiver/deleteCaregiver';

/*  This file is an interface for a service package, it will return a dictionary
**   containing all methods which can be accessed by service class
*/
export default (models) => ({
    user: {
        GetUser: (params) => userManipulation.GetUser(models, params),
        GetUsers: (params) => userManipulation.GetUsers(models, params),
        
        UpdateUser: (params) => userManipulation.UpdateUser(models, params),
        AddUser: (params) => userManipulation.AddUser(models, params),
        UpdateUserRole: (params) => userManipulation.UpdateUserRole(models, params),

        DeleteUser: (params) => userManipulation.DeleteUser(models, params)
    },
    medication: {
        AddMed: (params) => medManpilation.AddMed(models, params),

        GetMed: (params) => medManpilation.GetMed(models, params),
        GetMeds: (params) => medManpilation.GetMeds(models, params),

        UpdateMeds: (params) => medManpilation.UpdateMeds(models, params),
        DeleteMeds: (params) => medManpilation.DeleteMeds(models, params)
    },
    caregiver: {
        GetCaregivers: (params) => caregiverManipulation.GetCaregivers(models, params),
        DeleteCaregiver: (params) => caregiverManipulation.DeleteCaregiver(models, params)
    },
    pacient: {
        AddPacient: (params) => pacientManipulation.AddPacient(models, params),
    },

    // for includes & manipulation of data, acts like interface
    models: {
        pacients: models.pacient,
        user: models.user
    }
});