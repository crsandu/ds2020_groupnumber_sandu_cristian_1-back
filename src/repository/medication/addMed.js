/*  This function will fetch all pacients from a given
**  caregiver.
*/
export default (model, params) => {
    console.log(params);
    return model.med.create(params).then(() => {
        return {
            status: 200,
            message: {}
        };
    }).catch(err => {
        return {
            status: 401,
            message: {
                args: err.message
            }
        };
    });
}
