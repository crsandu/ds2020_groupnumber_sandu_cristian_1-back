import AddMed from './addMed'
import GetMed from './getMed'
import GetMeds from './getMeds'
import UpdateMeds from './updateMed'
import DeleteMeds from './deleteMeds'

export default {
    AddMed: (model, params) => AddMed(model, params),

    GetMed: (model, params) => GetMed(model, params),
    GetMeds: (model, params) => GetMeds(model, params),

    UpdateMeds: (model, params) => UpdateMeds(model, params),
    DeleteMeds: (model, params) => DeleteMeds(model, params)
}