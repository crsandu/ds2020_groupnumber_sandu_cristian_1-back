import assert from 'assert'

/*  This method will obtain only the first user which match in the database
**  the update value is kept in updateValue key.
*/
export default (model, params) => {
    return model.med.findOne(params)
    .then((value) => {
        assert(value != null, 'No medication found!');

        // at this moment only this field is updateable
        const replace = params.updateValue;
        value.update(replace);
        return {
            status: 200,
            message: {}
        };
    }).catch(err => {
        return {
            status: 400,
            message: {
                args: err.message
            }
        };
    })
}