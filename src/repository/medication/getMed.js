import assert from 'assert'

/*  This method will obtain only the first user which match in the database
**
*/
export default (model, params) => {
    console.log(params);
    return model.med.findOne(params)
    .then((value) => {
        assert(value != null, 'No medication found!');

        const result = {};
        params.reqParams.forEach(el => {result[el] = value[el]});
        return {
            status: 200,
            message: result
        };
    }).catch(err => {
        return {
            status: 400,
            message: {
                args: err.message
            }
        };
    })
}