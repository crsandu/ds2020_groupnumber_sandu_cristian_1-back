import assert from 'assert'

/*  This method will delete all medications from the database
**
*/
export default (model, params) => {
    return model.med.findAll(params)
    .then((value) => {
        assert(value != null, 'No medications found!');
        value.forEach(val => {
            val.destroy()
        });

        return {
            status: 200,
            message: {}
        };
    }).catch(err => {
        return {
            status: 400,
            message: {
                args: err.message
            }
        };
    })
}