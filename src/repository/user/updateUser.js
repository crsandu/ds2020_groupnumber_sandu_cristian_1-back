import assert from 'assert'

/*  This method will obtain only the first user which match in the database
**  the update value is kept in updateValue key.
*/
export default (model, params) => {
    return model.user.findOne(params)
    .then(async (value) => {
        assert(value != null, 'No user found!');

        // at this moment only this field is updateable
        const replace = params.updateValue;
        try{
            await value.update(replace);
        } catch (err) {
            return {
                status: 400,
                message: 'Could not update the entry!'
            };
        }

        return {
            status: 200,
            message: {}
        };
    }).catch(err => {
        console.log('PENIS');
        return {
            status: 400,
            message: {
                args: err.message
            }
        };
    })
}