import assert from 'assert'

function checkString(key) {
    const data = {
        Pacient: true,
        Doctor: true,
        Caregiver: true
    }
    console.log(key)
    return data[key];
}

/*  This method will obtain only the first user which match in the database
**  the update value is kept in updateValue key.
*/
export default (model, params) => {
    if(checkString(params.updateValue.role) !== true) {
        return {
            status: 400, 
            message: {
                args: 'Role is invalid!'
            }
        };
    }

    return model.user.findOne(params)
    .then((value) => {
        assert(value != null, 'No userfound found!');

        // at this moment only this field is updateable
        const replace = params.updateValue;
        value.update(replace);
        return {
            status: 200,
            message: {}
        };
    }).catch(err => {
        return {
            status: 400,
            message: {
                args: err.message
            }
        };
    })
}