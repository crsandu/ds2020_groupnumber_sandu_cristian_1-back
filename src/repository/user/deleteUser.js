import assert from 'assert'

/*  This method will delete all medications from the database
**
*/
export default (model, params) => {
    return model.user.findOne(params)
    .then((value) => {
        assert(value != null, 'No user found!');
        value.destroy();

        return {
            status: 200,
            message: {}
        };
    }).catch(err => {
        console.log(err);
        return {
            status: 400,
            message: {
                args: err.message
            }
        };
    })
}