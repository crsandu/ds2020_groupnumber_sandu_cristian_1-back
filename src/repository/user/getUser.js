import assert from 'assert'

/*  This method will obtain only the first user which match in the database
**
*/
export default (model, params) => {
    console.log(params)
    return model.user.findOne(params)
    .then((value) => {
        assert(value != null, 'No user found!');
        const result = {};
        console.log(value);

        params.reqParams.forEach(el => {result[el] = value[el]});
        console.log(value);
        return {
            status: 200,
            message: result
        };
    }).catch(err => {
        return {
            status: 400,
            message: {
                args: err.message
            }
        };
    })
}