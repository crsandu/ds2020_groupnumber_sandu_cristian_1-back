function checkString(key) {
    const data = {
        Pacient: true,
        Doctor: true,
        Caregiver: true
    }
    console.log(key)
    return data[key];
}


/*  This method is designed to add a uniquie
**  user in database
*/
export default (model, params) => {
    if(params.role !== undefined) {
        if(checkString(params.role) !== true) {
            return {
                status: 400, 
                message: {
                    args: 'Role is invalid!'
                }
            };
        }
    }

    return model.user.create(params).then(() => {
        return {
            status: 200,
            message: {}
        };
    }).catch(err => {
        return {
            status: 401,
            message: {
                args: err.message
            }
        };
    })
}