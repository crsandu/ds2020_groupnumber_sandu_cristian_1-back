import AddUser from './addUser'
import GetUser from './getUser'
import GetUsers from './getUsers'
import DeleteUser from './deleteUser'
import UpdateUserRole from './updateUserRole'
import UpdateUser from './updateUser'

/*  This script will manipulate the user data
*/
export default {
    GetUser: (model, params) => GetUser(model, params),
    AddUser: (model, params) => AddUser(model, params),

    UpdateUser: (model, params) => UpdateUser(model, params),
    GetUsers: (model, params) => GetUsers(model, params),
    CheckUser: (model, params) => GetUser(model, params),

    UpdateUserRole: (model, params) => UpdateUserRole(model, params),
    DeleteUser: (model, params) => DeleteUser(model, params)
} 